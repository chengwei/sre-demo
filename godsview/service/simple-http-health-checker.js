'use strict';

const AWS = require('aws-sdk')
const S3 = new AWS.S3()
const http = require('http');

module.exports.check = (event, context, callback) => {
  http.get(event.check_url, (resp) => {
    if (resp.statusCode == 200 ) {
      markHealthyTimestamp(process.env.S3_BUCKET, event.check_id)
    }
  }).on("error", (err) => {
    console.log("Error: " + err.message);
  });
};

function markHealthyTimestamp(s3_bucket, check_id) {
  let dateTime = Date.now()
  let current_time_in_sec = Math.floor(dateTime / 1000)

  S3.upload({
    Bucket: s3_bucket,
    Key: 'checks/' + check_id,
    ACL: 'public-read',
    Body: ''+current_time_in_sec
  }, (err, res) => {
    console.log(err, res)
  })
}